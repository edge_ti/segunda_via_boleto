# coding: utf-8

import random

class Itaucripto(object):
	sbox = range(256)
	key = range(256)
	TAM_COD_EMP = 26
	TAM_CHAVE = 16

	def algoritmo(self, string, string2):
		n = 0
		n2 = 0
		string3 = ""
		self.inicializa(string2)
		n3 = 1
		while n3 <= len(string):
			n = (n + 1) % 256
			n2 = (n2 + self.sbox[n]) % 256
			n4 = self.sbox[n]
			self.sbox[n] = self.sbox[n2]
			self.sbox[n2] = n4
			n5 = self.sbox[(self.sbox[n] + self.sbox[n2]) % 256]
			n6 = ord(string[n3 - 1]) ^ n5
			string3 = string3 + chr(n6)
			n3+=1
		return string3

	def inicializa(self, string):
		n = len(string)
		n2 = 0
		while n2 <= 255:
			self.key[n2] = ord(string[n2 % n])
			self.sbox[n2] = n2
			n2+=1
		n3 = 0
		n2 = 0
		while n2 <= 255:
			n3 = (n3 + self.sbox[n2] + self.key[n2]) % 256
			n4 = self.sbox[n2]
			self.sbox[n2] = self.sbox[n3]
			self.sbox[n3] = n4
			n2+=1

	def converte(self, string):
		c = chr(int(26.0 * random.random() + 65.0))
		string2 = "" + c
		n = 0
		while n < len(string):
			c3 = ord(string[n])
			c2 = c3
			string2 = string2 + str(c3)
			c = chr(int(26.0 * random.random() + 65.0))
			string2 = string2 + c
			n+=1
		return string2

	def geraCripto(self, chave_itau, segredo, cnpj, id_cliente):
		# if len(segredo) != self.TAM_COD_EMP:
		# 	return "Erro: tamanho do codigo da empresa diferente de 26 posi\u00e7\u00f5es."
		# if len(string3) != self.TAM_CHAVE:
		# 	return "Erro: tamanho da chave da chave diferente de 16 posi\u00e7\u00f5es."

		string4 = self.algoritmo(cnpj, id_cliente)
		string5 = self.algoritmo(segredo + string4, chave_itau)
		return self.converte(string5)
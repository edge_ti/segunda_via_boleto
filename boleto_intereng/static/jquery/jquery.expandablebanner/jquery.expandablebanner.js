/*Jquery Effects Core Como Parte da Solucao*/

jQuery.fn.extend({
	ExpandableBanner: function(options) {
		return this.each(function() {
			new jQuery.XpandableBanner(this, options);
		});
	}
});

var timer = 0;
var mouseOver = false;
jQuery.XpandableBanner = function(selectobj, options){
	var opt = options || {};
	if (!opt.startFrameUrl || !opt.endFrameUrl )
		throw "You must inform startFrameUrl and endFrameUrl properties.";	
	opt.bannerPosition = opt.bannerPosition || "top center";
	opt.initialWidth = opt.initialWidth || "500px";
	opt.initialHeight = opt.initialHeight || "60px";
	opt.finalWidth = opt.finalWidth || opt.initial_width;
	opt.finalHeight = opt.finalHeight || "200px";
	opt.easing = opt.easing || 'linear';
	opt.effectDuration = opt.effectDuration || 500;
	opt.click = opt.click || function(){};
	opt.title = opt.title || '';
	
	$(selectobj).css({width:opt.initialWidth, height: opt.initialHeight });
	
	var holder = document.createElement('div');
	$(holder)
	.attr('id', $(selectobj).attr('id') + '-jxholder')
	.attr('title', opt.title)
	.css({
		width:opt.initialWidth,
		height: opt.initialHeight,
		backgroundPosition: opt.bannerPosition,
		backgroundRepeat: 'no-repeat', 
		backgroundImage: 'url(' + opt.startFrameUrl + ')'})
	.addClass('expandablebanner')
	$(holder).appendTo(selectobj);
		
	function expand(){
		$(holder).css({backgroundImage: 'url(' + opt.endFrameUrl + ')'});		
		$(holder).animate({width: opt.finalWidth,height: opt.finalHeight}, opt.effectDuration, opt.easing);
	}
	
	function retract(){
		$(holder).animate({width:opt.initialWidth,height:opt.initialHeight},opt.effectDuration, opt.easing,endRetract);
	}
	function endRetract(){
		$(holder).css({
			backgroundImage: 'url(' + opt.startFrameUrl + ')'
		})
	};
	
	$(holder).mouseover(function(){
		if (!mouseOver){clearTimeout(timer);}  
		mouseOver = true;
		timer = setTimeout(expand,100);	
		
	});
	
	$(holder).mouseout(function(){
		if (mouseOver){clearTimeout(timer);}
		mouseOver = false;
		timer = setTimeout(retract,100);
	});
	
	$(holder).click(opt.click);
}

/******************* EASE OPTIONS ***********************
******************** JQuery default options				*
** linear												*
** swing												*
******************** JQuery UI options					*
** easeInQuad											*
** easeOutQuad											*
** easeInOutQuad										*
** easeInCubic											*
** easeOutCubic											*
** easeInOutCubic										*
** easeInQuart											*
** easeOutQuart											*
** easeInOutQuart										*
** easeInQuint											*
** easeOutQuint											*
** easeInOutQuint										*
** easeInSine											*
** easeOutSine											*
** easeInOutSine										*
** easeInExpo											*
** easeOutExpo											*
** easeInOutExpo										*
** easeInCirc											*
** easeOutCirc											*
** easeInOutCirc										*
** easeInElastic										*
** easeOutElastic										*
** easeInOutElastic										*
** easeInBack											*
** easeOutBack											*
** easeInOutBack										*
** easeInBounce											*
** easeOutBounce										*
** easeInOutBounce										*
******************* EASE OPTIONS ***********************/
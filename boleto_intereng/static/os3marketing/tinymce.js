function add_button_callback(ed, e) {
    // Add a custom button
    ed.addButton('contactButtom', {
        title : 'Adiciona nome do contato',
        image : '/media/os3marketing/user.gif',
        onclick : function() {
			ed.focus();
			ed.selection.setContent('{{contact.first_name}}');
        }
    });
}

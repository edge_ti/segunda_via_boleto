#coding: utf-8
from django.conf import settings
import cx_Oracle

def get_cx_connection(using=None):
    dsn_tns = cx_Oracle.makedsn(unicode(settings.DATABASES['ORACLE']['HOST']), int(settings.DATABASES['ORACLE']['PORT']), unicode(settings.DATABASES['ORACLE']['NAME']))
    return cx_Oracle.connect(unicode(settings.DATABASES['ORACLE']['USER']), unicode(settings.DATABASES['ORACLE']['PASSWORD']), dsn_tns)
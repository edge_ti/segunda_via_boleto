#coding: utf-8

from django import forms

class CnpjForm(forms.Form):
    cnpj = forms.CharField(max_length=18,required=True)
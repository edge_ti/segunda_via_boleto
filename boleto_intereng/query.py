#coding: utf-8

from utils import get_cx_connection

def getNomeporCnpj(cnpjcliente):
    conn = get_cx_connection('ORACLE')
    cur = conn.cursor()

    cur.prepare('Select Entnome From Entidade WHERE Entcpfcgc = :cnpj')
    r = cur.execute(None,{'cnpj':cnpjcliente})

    for row in r.fetchall():
        NomeEmpresa = row [0]
        r.close()
        return NomeEmpresa
    else:
        return "Inexistente"

def getDocsemAberto(cnpjcliente):
    conn = get_cx_connection('ORACLE')
    cur = conn.cursor()

    cur.prepare("Select Dof.Docfinchv, Dof.Docfinnum, Dof.Docfindatabasevencparc, Dof.Docfinvalor, Dof.Docfindataemissao From Doc_Fin Dof, Entidade Et,Parc_Doc_Fin Pf Where Dof.Entcod = Et.Entcod And Et.Entcpfcgc = :cnpj And   Dof.Docfintipolanc = 'REC' And Dof.Empcod = Pf.Empcod And Dof.Docfinchv = Pf.Docfinchv And Pf.Parcdocfinvalpag = 0")
    r = cur.execute(None,{'cnpj':cnpjcliente})

    for row in r.fetchall():
        Numdoc = row[1]
        ValDoc = row[3]
        return ({'Doc':Numdoc,'Valor':ValDoc})
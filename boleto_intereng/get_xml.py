
import urllib2, cookielib, urllib
from views import Itaucripto
from django.conf import settings

def get_dados_xml(url, cnpj):
    url = str(url).replace(" ", "+") # just in case, no space in url
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}

    req = urllib2.Request(url, headers=hdr)
    cripto = Itaucripto()
    dc = cripto.geraCripto(settings.CHAVE_ITAU, settings.SEGREDO, cnpj, settings.ID_CLIENTE)
    msg = "N"
    params = {"dados":dc,"msg":msg}
    query = urllib.urlencode(params)

    try:
        page = urllib2.urlopen(req, query)
        return page.read()
    except urllib2.HTTPError, e:
        print e.fp.read()
    return ''
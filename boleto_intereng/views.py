#coding: utf-8
from django.shortcuts import render, redirect
from itaucripto import Itaucripto
from forms import CnpjForm
from query import *
from get_xml import get_dados_xml
from xml.dom import minidom
from StringIO import StringIO



def index(request):
    return render(request,'index.html')


def segunda_via(request,template_name='segunda_via.html'):

    form = CnpjForm(request.POST)

    if form.is_valid():
        dados = request.POST.get('cnpj')

        #remove os caracteres especiais#
        dados = dados.replace(".","").replace("/","").replace("-","")

        xml = get_dados_xml("https://ww2.itau.com.br/2viabloq/2ViaXMLWebSvc.asmx/PesquisaXML",dados)
        reidentxml = minidom.parse(StringIO(xml))

        referro = reidentxml.getElementsByTagName("erro")

        if not referro:
            ref = reidentxml.getElementsByTagName("Bloqueto")
            nomeEmpresa = reidentxml.getElementsByTagName("sacado")[0]
            NomeEmp = nomeEmpresa.firstChild.data

            listadados = []
            for dados in ref:
                i = 0
                docref = dados.getElementsByTagName("NUMERODOCUMENTO")[i]
                dataref = dados.getElementsByTagName("VENCIMENTO")[i]
                valorref = dados.getElementsByTagName("VALOR")[i]
                linkref = dados.getElementsByTagName("LINK")[i]

                datacontext = {
                    'doc': docref.firstChild.data,
                    'datavenc': dataref.firstChild.data,
                    'valor': valorref.firstChild.data,
                    'link': linkref.firstChild.data
                }

                listadados.append(datacontext)

                retornocontexto = {
                    'dados': listadados,
                    'Empresa': NomeEmp
                }

                i + 1

            return render(request, template_name, retornocontexto)
        else:
            return render(request, template_name, {'erroboleto':'Não existem boletos a serem pagos ou o Cnpj digitado esta incorreto.'})

    else:
        return render(request, "index.html", {'erro':'Por favor digite um cnpj válido.'})
"""
Django settings for segunda_via_boleto project.

Generated by 'django-admin startproject' using Django 1.11.4.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7#^@4n6mue_d!k$-sn4_u)wfoh8xpublc$%7c5c173xh_@shnl'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['segundaviaboleto.intereng.com.br','www.customer.com.br']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    #Apps
    'boleto_intereng',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'segunda_via_boleto.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'segunda_via_boleto.context.default'
            ],
        },
    },
]

WSGI_APPLICATION = 'segunda_via_boleto.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    # 'ORACLE': {
    #     'ENGINE': 'django.db.backends.oracle', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
    #     'NAME': 'INTER',                      # Or path to database file if using sqlite3.
    #     'USER': 'apolo',                      # Not used with sqlite3.
    #     'PASSWORD': 'apolo',                 # Not used with sqlite3.
    #     'HOST': '172.16.20.6',           # Set to empty string for localhost. Not used with sqlite3.
    #     'PORT': '1521',
    # },
    'ORACLE': {
        'ENGINE': 'django.db.backends.oracle', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'LADDER',  # Or path to database file if using sqlite3.
        'USER': 'ladder',  # Not used with sqlite3.
        'PASSWORD': 'opala20',  # Not used with sqlite3.
        'HOST': '172.16.0.166',  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '1521',
    },
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

# intereng
# CHAVE_ITAU = "SEGUNDA12345ITAU"
# SEGREDO = "J0654727140001480000000001"
# ID_CLIENTE = "I1N23T4E5RENG6AU"

# ladder
CHAVE_ITAU = "SEGUNDA12345ITAU"
SEGREDO = "J0668861440001030000002332"
ID_CLIENTE = "EDGELADR20210325"

SITE = 'LADDER'
LOGO = '/static/imagens/ladder-pequeno.jpg'
EMPRESA_URL = 'http://ladder.com.br/'

ALLOWED_HOSTS = ['*']
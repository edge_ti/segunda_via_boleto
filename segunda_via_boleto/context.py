from django.conf import settings


def default(request):
    return {
        'boleto_title': settings.SITE,
        'boleto_logo': settings.LOGO,
        'empresa_url': settings.EMPRESA_URL
    }
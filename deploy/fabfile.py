# -*- coding: utf-8 -*-
import os
from contextlib import contextmanager
from fabric.api import *


@contextmanager
def source_virtualenv():
    with prefix(env.virtualenv):
        yield

def intereng():
    """"""
    env.hosts = ['172.16.20.20',]
    env.user = 'ladder'
    env.password = 'Ladder*123'
    env.deploy_to = '/producao/boleto_intereng/segunda_via_boleto'
    env.deploy_to_project = '%s/segunda_via_boleto' % env.deploy_to
    env.settings = '%s/deploy/producao/settings.intereng.py' % env.deploy_to
    env.manage = 'python %s/manage.py' % env.deploy_to
    env.virtualenv = 'source /producao/boleto_intereng/env_boleto_intereng/bin/activate'
    env.website = 'http://segundaviaboleto.intereng.com.br'
    env.restart = 'sudo kill -HUP `cat /tmp/segunda_via_boleto.pid`; exit'


def ladder():
    """"""
    env.hosts = ['172.16.0.28', ]
    env.user = 'administrador'
    env.password = 'w#zT&w7lbDl91%CE'
    env.deploy_to = '/mnt/dados/aplicacoes/boleto_ladder/segunda_via_boleto'
    env.deploy_to_project = '%s/segunda_via_boleto' % env.deploy_to
    env.settings = '%s/deploy/producao/settings.ladder.py' % env.deploy_to
    env.manage = 'python %s/manage.py' % env.deploy_to
    env.virtualenv = 'source /mnt/dados/aplicacoes/boleto_ladder/env_boleto_ladder/bin/activate'
    env.website = 'http://segundaviaboleto.ladder.com.br'
    env.restart = 'sudo kill -HUP `cat /tmp/segunda_via_boleto_ladder.pid`; exit'


def deploy_config():
    """replace all settings"""
    run('sudo mv -f %s %s/settings.py' % (env.settings, env.deploy_to_project))

def deploy():
    with source_virtualenv():
        clean()
        deploy_config()
        static_files()
        restart()
        # check()

def update():
    open_shell(command='cd %s;git reset --hard; sudo git pull origin master;exit' % env.deploy_to)

def static_files():
    """collect static files"""
    run('%s %s'  % (env.manage,'collectstatic --noinput'))
    open_shell(command='cd %s; sudo chown -R www-data:www-data staticfiles; exit' % env.deploy_to)

def migrate_and_syncdb():
    """syncdb"""
    run('%s %s'  % (env.manage,'migrate'))

def clean ():
    """remove compiled files"""
    run('find %s -name \*.pyc | xargs rm -f ' % env.deploy_to)

def restart():
    """run e sudo command were failing so i tried this new feature and it worked properly"""
    open_shell(command=env.restart)

def check():
    print('Checking site status...')
    out = run('curl --silent -I "%s"' % env.website)
    if '200 OK' in out:
        print('\n\n\n\n\n\nLOOKS GOOD HERE! WEB SITE IS UP\n\n\n\n\n\n')
    else:
        print('\n\n\n\n\n\n\nNOT WORKING!!! ):\n\n\n\n\n\n')

